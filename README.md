# SA Remittances

## [1) lvc-feature-implementation.py](https://gitlab.com/cada-python/sa-remittances/-/blob/master/lvc-feature-implementation.py)
This file implements the ***churn prediction model*** on the lvc(low value customer) base of SA Remittances.Features are built from the transactional data of the last 3 months of the SA Remittances customers and are fed in the trained model to predict whether or not the lvc customers are going to be active in the next three months.Data is originating from the concated daily `CSVs` into a dataframe called `sa_remittance_data`and the destination database is `sa_remittances` and table name `churn_prediction_lvc`.<br>

**CHURN PREDICTION MODEL INPUT FEATURES**
1. total_transactions
2. agent_id_CH29302987
3. agent_id_CH95437052
4. payment_method_EF
5. payment_method_PA
6. max_lag_on_last_date
7. sum_lag_more_than_day
8. sum_transactions_status_cancelled
9. average_monthly_transaction_value_rand
10. active_months
11. segments_changes
12. nunique_beneficiaries
13. gender_male
14. sender_age
15. sender_remittance_age

***Note that data is aggregated on the sender_accountname***



## [2) worldbank-data-cleansed-lattest.py](https://gitlab.com/cada-python/sa-remittances/-/blob/master/worldbank-data-cleaned-latest.py)

This file takes an excel sheet `rpw_dataset_2011_2020_q2_v1.xlsx` with world over remit data put together by the world bank and store it  in a dataframe.The dataframe is cleansed by replacing null fields which will be categorised and replacing the missing source regions.


**OUTPUT COLUMNS**
1. id
2. date
3. period
4. source_name
5. source_region
6. destination_name 
7. destination_region
8. corridor
9. firm
10. receiving network coverage
11. firm_type 
12. payment instrument
13. access point
14. speed actual
15. cc1 total cost 
16. cc2 total cost 
17. cc1 denomination amount
18. cc2 denomination amount

The `csv` file is updated every quater by the wordbank and then import it in our database `sa_remittances`.







    
